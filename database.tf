

resource "mysql_database" "this" {
  name = var.database_name
}
resource "mysql_user" "this" {
  user               = var.database_name
  plaintext_password = var.database_password
  host = "%"
}

resource "mysql_grant" "this" {
  user       = mysql_user.this.user
  host       = "%"
  database   = mysql_database.this.name
  privileges = ["ALL"]
}

variable "database_name" {
  description = "The database Name"
}
variable "database_password" {
  description = "The database password"
}

